﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Health : MonoBehaviour {

	public Image[] cells;
	uint currentCell = 0;
	public int numCells;

	// Use this for initialization
	void Start () {

		cells = GetComponentsInChildren<Image>();
		currentCell = 0;
		numCells = 3;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StrikeCell(ref uint numPlayers)
	{
		cells[currentCell].color = Color.black;
		currentCell++;
		if(currentCell == 3)
		{
			numPlayers--;
		}
	}


}
