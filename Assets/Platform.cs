﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

	// Use this for initialization
	public Color color;
	Renderer render;

	void Start () {
		render = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetColor(WorldHandler.player_t.teamName team)
	{
		switch(team)
		{
			case WorldHandler.player_t.teamName.red:
				{
					color = Color.red;
					break;
				}
			case WorldHandler.player_t.teamName.blue:
				{
					color = Color.blue;
					break;
				}

			case WorldHandler.player_t.teamName.green:
				{
					color = Color.green;
					break;
				}
		}
		render.material.SetColor("_EmissionColor", color);
	}
}
