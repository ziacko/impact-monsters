﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldHandler : MonoBehaviour {

	// Use this for initialization
	public GameObject spawnerPrefab;
	public GameObject specialSpawnerPrefab;
	public GameObject HUDPrefab;
	public GameObject HUD;
	public HUDScore[] scores;
	public Camera gameCamera;
	public Menu intro;
	public AudioSource sources;
	public AudioClip[] sounds;
	public AudioClip music;
	Clock gameClock;
	Ending ending;

	public float gameTimer = 30.0f;
	public float currentGameTimer = 0.0f;

	float introTimer = 0.0f;

	public enum gameState_t
	{
		INTRO,
		DEFAULT,
		MENU,
		END,
	};

	public gameState_t currentState;
	public class player_t
	{
		public enum teamName
		{
			red,
			green,
			blue,
			none,
		};

		public player_t(int health = 3)
		{
			this.health = health;
			spawner = null;
			score = 0;
			name = teamName.none;
		}

		public int health;
		public uint score;
		public monsterSpawner spawner;
		public teamName name;
	};

	public List<player_t> players;
	Health[] playerHealths;
	uint numPlayers;
	bool isPaused;

	public SpecialSpawner specialSpawner;

	public bool showCursor = true;

	// Use this for initialization
	void Start () {
		//monsters = GetComponentsInChildren<Monster>();
		scores = GetComponentsInChildren<HUDScore>();
		ending = GetComponentInChildren<Ending>();
		intro = GetComponentInChildren<Menu>();
		playerHealths = GetComponentsInChildren<Health>();
		players = new List<player_t>();
		numPlayers = 3;
		isPaused = false;
		for (int i = 0; i < 3; i++)
		{
			player_t player = new player_t();
			GameObject tempSpawner = Instantiate(spawnerPrefab,
				new Vector3(), Quaternion.Euler(new Vector3(0, 180, 0))) as GameObject;
			tempSpawner.transform.parent = this.transform;
			tempSpawner.transform.localPosition = new Vector3(-10 + (7.5f * i), 0.2f, 30);
			player.spawner = tempSpawner.GetComponent<monsterSpawner>();
			player.spawner.team = WorldHandler.player_t.teamName.red + i;
			players.Add(player);

			scores[i].SetColor(WorldHandler.player_t.teamName.red + i);
		}

		GameObject tempSpecial = Instantiate(specialSpawnerPrefab, new Vector3(), Quaternion.Euler(new Vector3(0, 180, 0))) as GameObject;
		tempSpecial.transform.parent = this.transform;
		tempSpecial.transform.localPosition = new Vector3(-10 + 7.5f, 4.5f, 30);
		specialSpawner = tempSpecial.GetComponent<SpecialSpawner>();

		intro.movie.Play();
		currentState = gameState_t.INTRO;
		gameClock = GetComponentInChildren<Clock>();
		ending.Hide(false);
	}
	
	// Update is called once per frame
	void Update () {
		switch (currentState)
		{
			case gameState_t.DEFAULT:
				{
					if (!isPaused)
					{						
						if(Input.GetKeyDown(KeyCode.Space))
						{
							isPaused = true;
							PauseMonsters(false);
						}

						else if(Input.GetKeyDown(KeyCode.Return))
						{
							showCursor = !showCursor;
							Cursor.visible = showCursor;
						}

					if (currentGameTimer < gameTimer)
					{
						currentGameTimer += Time.deltaTime;
						gameClock.SetTime(gameTimer - currentGameTimer);
							if (Input.GetMouseButtonDown(0))
							{
								RaycastHit hit = new RaycastHit();
								Ray mouseRay = gameCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
								if (Physics.Raycast(mouseRay, out hit))
								{
									if (hit.collider.gameObject.transform.parent != null)
									{
										Monster monster = hit.collider.gameObject.transform.parent.GetComponent<Monster>();
										if (monster != null)
										{
											if (monster.isVulnerable)
											{
												if (monster.health > 1)
												{
													monster.ChangeState(Monster.state_t.damaged);
												}
												else
												{
													monster.ChangeState(Monster.state_t.dead);
												}

												players[(int)monster.team].score += 10;
												monster.health--;
												scores[(int)monster.team].SetScore(players[(int)monster.team].score);
											}
										}

										else
										{
											Special specialMonster = hit.collider.gameObject.transform.parent.GetComponent<Special>();
											if(specialSpawner != null)
											{
												players[(int)specialSpawner.specialMonster.team].score += 100;
												scores[(int)specialSpawner.specialMonster.team].SetScore(players[(int)specialSpawner.specialMonster.team].score);
												specialSpawner.KillMonster();
											}
										}
									}
								}
							}
						}
						else
						{
							ShowWinner();
						}
					}

					else
					{
						if (Input.GetKeyDown(KeyCode.Space))
						{
							isPaused = false;
							PauseMonsters(true);
						}
					}

					break;
				}

			case gameState_t.INTRO:
				{
					if (introTimer < intro.movie.duration)
					{
						introTimer += Time.deltaTime;
					}

					else
					{
						//intro.gameObject.SetActive(false);
						intro.HideIntro(false);
						currentState = gameState_t.DEFAULT;
						gameClock.isTicking = true;
						for(int i = 0; i < players.Count; i++)
						{
							players[i].spawner.PauseMonsters(true);
						}
					}

					break;
				}

			case gameState_t.MENU:
				{
					break;
				}

			case gameState_t.END:
				{
					if(Input.GetKeyDown(KeyCode.Escape))
					{
						Application.Quit();
					}
					break;
				}
		}
	}

	public void PauseMonsters(bool shouldPause)
	{
		for(int i = 0; i < 3; i++)
		{
			players[i].spawner.PauseMonsters(shouldPause);
		}
		specialSpawner.Pause(shouldPause);
	}

	public void HitPlayer(int playerIndex)
	{
		playerHealths[playerIndex].StrikeCell(ref numPlayers);
		players[playerIndex].health--;
		//numPlayers--;
		if(players[playerIndex].health == 0)
		{
			players[playerIndex].spawner.gameObject.SetActive(false);
			
		}
		if (numPlayers == 0)
		{
			ShowWinner();
		}
	}

	void ShowWinner()
	{
		//post game. find the winning team and display winner
		gameClock.isTicking = false;
		//get highest manually
		uint highest = 0;
		int winningMonster = 0;
		bool draw = false;
		for (int i = 0; i < players.Count; i++)
		{
			players[i].spawner.PauseMonsters(false);
			specialSpawner.Pause(false);

			if (players[i].score > highest)
			{
				//set the new highest
				winningMonster = i;
				highest = players[i].score;
				draw = false;
			}
			//this doesnt seem to make much sense
			else if (players[i].score == highest)
			{
				//draw!
				draw = true;
			}
		}

		ending.Hide(true);
		currentState = gameState_t.END;
		if (draw)
		{
			//if there is a draw then display draw
			ending.SetWinner(player_t.teamName.none);
		}

		else
		{
			//else show the winning player
			//display the winning player
			ending.SetWinner(players[winningMonster].spawner.team);
		}
	}
}
