﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDScore : MonoBehaviour {

	//get image and text. and methods for settings these
	Text text;
	Image image;

	// Use this for initialization
	void Start () {
		image = GetComponent<Image>();
		text = GetComponentInChildren<Text>();	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetColor(WorldHandler.player_t.teamName color)
	{
		switch (color)
		{
			case WorldHandler.player_t.teamName.red:
				{
					image.color = Color.red;
					break;
				}

			case WorldHandler.player_t.teamName.blue:
				{
					image.color = Color.blue;
					break;
				}

			case WorldHandler.player_t.teamName.green:
				{
					image.color = new Color(0.0f, 0.5f, 0.0f);
					break;
				}
		}
	}

	public void SetScore(uint score)
	{
		text.text = "Score: " + score.ToString();
	}
}
