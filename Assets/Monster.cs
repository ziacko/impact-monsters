﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour {

	public WorldHandler.player_t.teamName team;
	public WorldHandler handler;

	public bool isVulnerable;
	public bool isAnimating;

	//Platform platform;
	Animation animations;
	string currentAnimation;
	float animationTimer = 0.0f;
	state_t currentState;

	Renderer render;
	Color defaultColor;

	Vector3 cameraPos;

	public float speed = 3.0f;

	public int maxHealth = 2;
	public int health;
	public float fadeSpeed = 3.0f;
	bool isPaused = false;
	AudioSource source;
	public AudioClip damagedSound;
	public AudioClip deathSound;
	public AudioClip attackSound;

	public enum state_t
	{
		moving,
		damaged,
		attacking,
		dead
	};

	//pick all the appropriate animations and pick them at random
	//if the animation is what makes the monster vulnerable
	//then 
	void Start () {
		currentState = state_t.moving;
		isVulnerable = false;
		isAnimating = true;
		//platform = GetComponentInChildren<Platform>();
		animations = GetComponent<Animation>();
		render = GetComponentInChildren<Renderer>();
		defaultColor = render.material.GetColor("_Color");
		cameraPos = GameObject.Find("Origin").transform.position;
		maxHealth = 2;
		health = maxHealth;
		ChangeState(state_t.moving);
		handler = GameObject.FindObjectOfType<WorldHandler>();
		source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (isAnimating)
		{
			if (animationTimer < animations[currentAnimation].length)
			{
				animationTimer += Time.deltaTime;
				if (currentState == state_t.moving)
				{
					transform.localPosition -= transform.forward * (Time.deltaTime * speed);

					//if the alien is too close to the play camera?
					if (Vector3.Distance(cameraPos, transform.position) < 10)
					{
						ChangeState(state_t.attacking);
					}
				}
			}

			else
			{
				switch (currentState)
				{
					case state_t.attacking:
						{
							//when the attack has ended. finish the game
							//Application.Quit();
							ChangeState(state_t.attacking);
							break;
						}

					case state_t.damaged:
						{
							ChangeState(state_t.moving);
							break;
						}

					case state_t.moving:
						{
							animationTimer = 0.0f;
							break;
						}
				}
			}
		}

		else 
		{
			if (currentState == state_t.dead)
			{
				defaultColor.a -= Time.deltaTime * 0.5f;
				render.material.SetColor("_Color", defaultColor);

				if (defaultColor.a < 0)
				{
					//destroy self
					Destroy(gameObject);
				}
			}
		}
	}

	public void ChangeState(state_t state)
	{
		this.currentState = state;

		switch (currentState)
		{
			case state_t.attacking:
				{
					//play the claw animation?
					ChangeAnimation("jumpBite", WrapMode.Loop);
					source.clip = attackSound;
					source.Play();
					handler.HitPlayer((int)team);
					break;
				}

			case state_t.damaged:
				{
					//push back quite a bit!
					transform.localPosition -= transform.forward * 5.0f;
					source.clip = damagedSound;
					source.Play();
					ChangeAnimation("getHit", WrapMode.Once);
					isVulnerable = false;
					//play the get damaged animation?
					break;
				}

			case state_t.moving:
				{
					ChangeAnimation("walk", WrapMode.Loop);
					isVulnerable = true;
					//play the moving animation?
					break;
				}

			case state_t.dead:
				{
					ChangeAnimation("death", WrapMode.Once);
					source.clip = deathSound;
					source.Play();
					isVulnerable = false;
					isAnimating = false;
					break;
				}
		}

	}

	AnimationClip GetClipByIndex(int index)
	{
		int i = 0;
		foreach (AnimationState animationState in animations)
		{
			if (i == index)
				return animationState.clip;
			i++;
		}
		return null;
	}

	void ChangeAnimation(string animationString, WrapMode mode)
	{
		animationTimer = 0.0f;
		currentAnimation = animationString;
		animations.Play(animationString);
		animations.wrapMode = mode;		
	}

	public void SetTeam(int teamName)
	{
		render = GetComponentInChildren<Renderer>();
		team = WorldHandler.player_t.teamName.red + teamName;

		switch(team)
		{
			case WorldHandler.player_t.teamName.red:
				{
					defaultColor = Color.red;
					break;
				}
			case WorldHandler.player_t.teamName.blue:
				{
					defaultColor = Color.blue;
					break;
				}

			case WorldHandler.player_t.teamName.green:
				{
					defaultColor = Color.green;
					break;
				}
		}
		render.material.SetColor("_Color", defaultColor);
	}

	public void Pause(bool shouldPause)
	{
		if (animations != null)
		{
			animations.enabled = shouldPause;
			isAnimating = shouldPause;
		}
	}
}
