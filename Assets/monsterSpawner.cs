﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class monsterSpawner : MonoBehaviour {

	//spawn position. this gameobject location
	
	//spawn frequency
	public float frequency;
	float currentSpawnTimer = 0.0f;
	//spawn start
	public Vector3 start;
	//spawn end
	public Vector3 end;

	public float span = 3.75f;

	List<Monster> monsters;
	public bool keepSpawning;
	int maxNumMonsters;
	public WorldHandler.player_t.teamName team;

	// Use this for initialization
	void Start () {
		keepSpawning = true;
		maxNumMonsters = 5;
		//frequency = 3.0f;
		Random.InitState(123);
		monsters = new List<Monster>();
	}
	
	// Update is called once per frame
	void Update () {

		//spawn a new monster every few seconds until the game ends
		if(keepSpawning)
		{
			if(currentSpawnTimer < frequency)
			{
				currentSpawnTimer += Time.deltaTime;
			}

			else
			{
				currentSpawnTimer = 0.0f;
				SpawnMonster();
			}
		}

		else
		{

		}
	}

	void SpawnMonster()
	{
		float monsterPos = Random.Range(transform.position.x - span, transform.position.x + span);
		GameObject tempMonster = Instantiate(Resources.Load("Monster"), new Vector3(), Quaternion.Euler(new Vector3(0, 180, 0))) as GameObject;
		tempMonster.transform.parent = this.transform;
		tempMonster.transform.position = new Vector3(monsterPos, transform.position.y, transform.position.z);
		monsters.Add(tempMonster.GetComponent<Monster>());
		tempMonster.GetComponent<Monster>().SetTeam((int)team);
	}

	public void PauseMonsters(bool shouldPause)
	{
		keepSpawning = shouldPause;
		foreach(Monster monster in monsters)
		{
			if (monster != null)
			{
				monster.Pause(shouldPause);
			}
		}
	}
}
