﻿using UnityEngine;
using System.Collections;

public class SpecialSpawner : MonoBehaviour {

	public Special specialMonster;

	public float spawnCooldown = 5.0f;
	float currentSpawnCooldown = 0.0f;
	bool onCooldown;

	// Use this for initialization
	void Start () {
		onCooldown = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (!onCooldown)
		{
			if (currentSpawnCooldown < spawnCooldown)
			{
				currentSpawnCooldown += Time.deltaTime;
			}

			else
			{
				currentSpawnCooldown = 0.0f;
				SpawnNewMonster();
				//onCooldown = false;
				//spawn monster
			}
		}
	}

	void SpawnNewMonster()
	{
		onCooldown = true;
		//spawn a new monster at this position
		GameObject temp = Instantiate(Resources.Load("Special"), new Vector3(), Quaternion.Euler(new Vector3(0, 180, 0))) as GameObject;
		temp.transform.parent = transform;
		specialMonster = temp.GetComponent<Special>();

	}

	public void KillMonster()
	{
		Destroy(specialMonster.gameObject);
		onCooldown = false;
		//after the monster is killed use a 5 second cooldown
	}

	public void Pause(bool shouldPause)
	{
		specialMonster.Pause(shouldPause);
	}
}
