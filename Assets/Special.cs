﻿using UnityEngine;
using System.Collections;

public class Special : MonoBehaviour {

	public WorldHandler.player_t.teamName team;
	WorldHandler.player_t.teamName previousTeam;
	public WorldHandler handler;

	bool isPaused = false;
	public float speed = 1.0f;
	Vector4 farLeft;
	Vector4 farRight;
	float progress = 0.0f;

	Renderer render;

	public float teamSwapCooldown = 1.0f;
	float currentSwapCooldown;

	// Use this for initialization
	void Start () {
		render = GetComponentInChildren<Renderer>();
		farLeft = new Vector3(-25, 0, 0);
		farRight = new Vector3(25, 0, 0);
		previousTeam = WorldHandler.player_t.teamName.none;
		team = WorldHandler.player_t.teamName.none;
		currentSwapCooldown = 0.0f;
		SetTeam();
	}
	
	// Update is called once per frame
	void Update () {
		if (!isPaused)
		{
			//lerp from left to right on a sine wave
			progress += Time.deltaTime * speed;
			float position = 0.0f;// Mathf.Cos(progress);
			position = Mathf.PingPong(progress, 1);
			//position = Mathf.Clamp01(position);
			//Debug.Log(position.ToString());
			transform.localPosition = Vector3.Lerp(farLeft, farRight, position);

			if (currentSwapCooldown < teamSwapCooldown)
			{
				currentSwapCooldown += Time.deltaTime;
			}

			else
			{
				currentSwapCooldown = 0.0f;
				SetTeam();
			}
		}
	}

	public void SetTeam()
	{
		WorldHandler.player_t.teamName tempTeam = (WorldHandler.player_t.teamName)Random.Range((int)WorldHandler.player_t.teamName.red, (int)WorldHandler.player_t.teamName.blue + 1);

		//if the team is the same as the previous team then re-roll
		if(tempTeam == previousTeam)
		{
			//use recursion
			SetTeam();
		}

		else
		{
			team = tempTeam;
			render = GetComponentInChildren<Renderer>();
			Color newColor = Color.black;
			switch(team)
			{
				case WorldHandler.player_t.teamName.red:
					{
						newColor = Color.red;
						break;
					}

				case WorldHandler.player_t.teamName.green:
					{
						newColor = Color.green;
						break;
					}

				case WorldHandler.player_t.teamName.blue:
					{
						newColor = Color.blue;
						break;
					}
			}
			render.material.SetColor("_Color", newColor);
		}
	}

	public void Pause(bool shouldPause)
	{
		isPaused = shouldPause;
	}
}
