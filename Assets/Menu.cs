﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour {

	public MovieTexture movie;
	GameObject intro;

	void Start () {

		intro = GameObject.Find("Intro");
		GetComponent<RawImage>().texture = movie as MovieTexture;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void HideIntro(bool hide)
	{
		intro.SetActive(hide);
	}
}
