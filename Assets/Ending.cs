﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Ending : MonoBehaviour {

	Text text;
	Image image;
	// Use this for initialization
	void Start () {
		text = GetComponentInChildren<Text>();
		image = GetComponent<Image>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Hide(bool show)
	{
		image.enabled = show;
		text.enabled = show;
	}

	public void SetWinner(WorldHandler.player_t.teamName team)
	{
		text.text = "Team " + team.ToString() + " has won!";
		switch (team)
		{
			case WorldHandler.player_t.teamName.red:
				{
					image.color = Color.red;
					break;
				}

			case WorldHandler.player_t.teamName.blue:
				{
					image.color = Color.blue;
					break;
				}

			case WorldHandler.player_t.teamName.green:
				{
					image.color = new Color(0.0f, 0.5f, 0.0f);
					break;
				}

			case WorldHandler.player_t.teamName.none:
				{
					image.color = Color.black;
					text.text = "Draw!";
					break;
				}
		}
	}
}
